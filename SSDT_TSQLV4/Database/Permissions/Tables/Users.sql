﻿CREATE TABLE [Permissions].[Users]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Name] varchar(50) not null,
	[Password] varbinary(500) not null,
	[DateOfRegister] datetime not null
)
