﻿CREATE TABLE [Permissions].[UsersRoles]
(
	[UserId] int not null,
	[RoleId] int not null,
	constraint PK_UsersRoles primary key clustered (UserId, RoleId),
	constraint FK_UsersRoles_Users foreign key(UserId) references Permissions.Users(Id),
	constraint FK_UsersRoles_Roles foreign key(RoleId) references Permissions.Roles(Id)
)


