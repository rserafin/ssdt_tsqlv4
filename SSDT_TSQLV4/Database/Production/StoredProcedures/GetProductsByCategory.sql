﻿CREATE PROCEDURE [dbo].[GetProductsByCategory]
	@categoryId int
AS
BEGIN
	SELECT p.productid as Id,p.productname as Name 
	FROM Production.Products p
	WHERE p.categoryid = @categoryId
END
